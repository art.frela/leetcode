package main

import (
	"fmt"
	"math"
)

func main() {
	input := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(twoSum(input, 15))
}

func twoSum(nums []int, target int) []int {
	hiBorder := math.Pow10(9)
	lowBorder := -math.Pow10(9)
	if len(nums) < 2 || float64(target) <= lowBorder || float64(target) >= hiBorder {
		return nil
	}
	for i, n := range nums {
		if float64(n) <= lowBorder || float64(n) >= hiBorder {
			continue
		}
		for j, m := range nums[i+1:] {
			if float64(m) <= lowBorder || float64(m) >= hiBorder {
				continue
			}
			if n+m == target {
				return []int{i, i + 1 + j}
			}
		}
	}
	return nil
}

func twoSum2hash(nums []int, target int) []int {
	hiBorder := math.Pow10(9)
	lowBorder := -math.Pow10(9)
	if len(nums) < 2 || float64(target) <= lowBorder || float64(target) >= hiBorder {
		return nil
	}
	cross := make(map[int]int)
	for i, n := range nums {
		if float64(n) <= lowBorder || float64(n) >= hiBorder {
			continue
		}
		cross[n] = i
	}
	for i, n := range nums {
		remain := target - n
		if ix, ok := cross[remain]; ok && ix != i {
			return []int{i, ix}
		}
	}
	return nil
}

func twoSum1hash(nums []int, target int) []int {
	hiBorder := math.Pow10(9)
	lowBorder := -math.Pow10(9)
	if len(nums) < 2 || float64(target) <= lowBorder || float64(target) >= hiBorder {
		return nil
	}
	cross := make(map[int]int)
	for i, n := range nums {
		if float64(n) <= lowBorder || float64(n) >= hiBorder {
			continue
		}
		remain := target - n
		if ix, ok := cross[remain]; ok {
			return []int{ix, i}
		}
		cross[n] = i
	}
	return nil
}
