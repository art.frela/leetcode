package main

import (
	"reflect"
	"testing"
)

func TestTwoSum(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"simple_0-9_10", args{nums: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 10}, []int{1, 9}},
		{"simple_0-9_9", args{nums: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 9}, []int{0, 9}},
		{"simple_1-9_4", args{nums: []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 4}, []int{0, 2}},
		{"simple_2-7-11-15_9", args{nums: []int{2, 7, 11, 15}, target: 9}, []int{0, 1}},
		{"empty_2-7-11-15_99", args{nums: []int{2, 7, 11, 15}, target: 99}, nil},
		{"with_big_digit_0", args{nums: []int{1000000001, -7, -1000000001, 7}, target: 0}, []int{1, 3}},
		{"with_big_digit_nil", args{nums: []int{1000000001, -1000000000, -1000000001, 1000000000}, target: 0}, nil},
		{"long_slice_4", args{nums: []int{10, 20, 30, 50, 60, 90, 100, 11, 22, 33,
			44, 55, 66, 77, 88, 99, 112, 123, 134, 145,
			156, 167, 178, 189, 190, 213, 225, 236, 247, 258,
			269, 271, 282, 294, 314, 325, 336, 347, 358, 369,
			371, 382, 393, 444, 555, 676, 777, 888, 999, 1234},
			target: 999 + 134}, []int{18, 48}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSum(tt.args.nums, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("twoSum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTwoSum2hash(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"simple_0-9_10", args{nums: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 10}, []int{1, 9}},
		{"simple_0-9_9", args{nums: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 9}, []int{0, 9}},
		{"simple_1-9_4", args{nums: []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 4}, []int{0, 2}},
		{"simple_2-7-11-15_9", args{nums: []int{2, 7, 11, 15}, target: 9}, []int{0, 1}},
		{"empty_2-7-11-15_99", args{nums: []int{2, 7, 11, 15}, target: 99}, nil},
		{"with_big_digit_0", args{nums: []int{1000000001, -7, -1000000001, 7}, target: 0}, []int{1, 3}},
		{"with_big_digit_nil", args{nums: []int{1000000001, -1000000000, -1000000001, 1000000000}, target: 0}, nil},
		{"long_slice_4", args{nums: []int{10, 20, 30, 50, 60, 90, 100, 11, 22, 33,
			44, 55, 66, 77, 88, 99, 112, 123, 134, 145,
			156, 167, 178, 189, 190, 213, 225, 236, 247, 258,
			269, 271, 282, 294, 314, 325, 336, 347, 358, 369,
			371, 382, 393, 444, 555, 676, 777, 888, 999, 1234},
			target: 999 + 134}, []int{18, 48}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSum2hash(tt.args.nums, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("twoSum2hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTwoSum1hash(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"simple_1-9_4", args{nums: []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, target: 4}, []int{0, 2}},
		{"simple_2-7-11-15_9", args{nums: []int{2, 7, 11, 15}, target: 9}, []int{0, 1}},
		{"empty_2-7-11-15_99", args{nums: []int{2, 7, 11, 15}, target: 99}, nil},
		{"with_big_digit_0", args{nums: []int{1000000001, -7, -1000000001, 7}, target: 0}, []int{1, 3}},
		{"with_big_digit_nil", args{nums: []int{1000000001, -1000000000, -1000000001, 1000000000}, target: 0}, nil},
		{"long_slice_4", args{nums: []int{10, 20, 30, 50, 60, 90, 100, 11, 22, 33,
			44, 55, 66, 77, 88, 99, 112, 123, 134, 145,
			156, 167, 178, 189, 190, 213, 225, 236, 247, 258,
			269, 271, 282, 294, 314, 325, 336, 347, 358, 369,
			371, 382, 393, 444, 555, 676, 777, 888, 999, 1234},
			target: 999 + 134}, []int{18, 48}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSum1hash(tt.args.nums, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("twoSum1hash() = %v, want %v", got, tt.want)
			}
		})
	}
}
